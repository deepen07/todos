const todaydate = new Date();
const days = new Array( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" );
const months = new Array("January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December");

document.getElementsByClassName("date-day")['0'].innerHTML = months[todaydate.getMonth()];
document.getElementsByClassName("date-day")['1'].innerHTML = todaydate.getDate();
document.getElementsByClassName("date-day")['2'].innerHTML = days[todaydate.getDay() - 1];
document.getElementById("today-time").innerHTML = todaydate.getHours() + ":" + todaydate.getMinutes();
var ul = document.getElementById("todo-lists");
var inputDOM = document.getElementById("todoinput");
var todolists = localStorage.getItem('mytodos') == 'null'?[]:JSON.parse(localStorage.getItem('mytodos'))
function addtodo(){
    inputValue = inputDOM.value;
    if(inputValue == ""){
        alert("Please enter To Do");
    } else {
         todolists.push({
             completed: false,
             task: inputValue
         })
        var todoJSON = JSON.stringify(todolists);
        localStorage.setItem('mytodos', todoJSON);
        process_to_dos()
    }
        
}

function editFunction(index){
    var editbtn = document.getElementById("editMe"+index);
    if (editbtn.innerHTML === "Edit Me") {
        editbtn.innerHTML = "Done";
        inputDOM.value = todolists[index].task;
    } else {
        editbtn.innerHTML = "Edit Me";
        todolists[index].task = inputDOM.value;
        document.getElementById(index).innerHTML = inputDOM.value;
        inputDOM.value = "";
        var todoJSON = JSON.stringify(todolists);
        localStorage.setItem('mytodos', todoJSON);
    }
}

function deleteTask(index){
    var delTodo = todolists[index];
    var indexs = todolists.indexOf(delTodo);
    if (indexs > -1) {
        todolists.splice(indexs, 1);
    }
    var todoJSON = JSON.stringify(todolists);
    localStorage.setItem('mytodos', todoJSON);
    process_to_dos();
}

function taskCompleted(index){
    todolists[index].completed = true;
    var comp = todolists[index].completed;
    if(comp == true){
        document.getElementById(index).classList.add("completed");
    }
    var todoJSON = JSON.stringify(todolists);
    localStorage.setItem('mytodos', todoJSON);
    console.log(comp);
}
console.log(todolists);
// console.log(todolists[index].completed);

function KeyPress(e) { 
    e = e || window.event;
    var key = e.keyCode ? e.keyCode : e.which;
    if (key == 13) {
        e.preventDefault();
        var editbtn = document.getElementById("editMe"+index);
        if (editbtn.innerHTML === "Edit Me") {
            addtodo();
            
        } else {
            editFunction();
        }
        document.getElementById("todoinput").value = '';
    }
    
}

function process_to_dos(){
    ul.innerHTML = '';
    for (index in todolists){
        var li, taskNode;
        li = document.createElement("li");
        var icon = document.createElement("i");
        taskNode = document.createTextNode(todolists[index].task);
        var complete = document.createElement("input");
        var editText = document.createElement("a");
        var editbtn = document.createTextNode("Edit Me");
        editText.setAttribute("id", "editMe"+index)
        editText.appendChild(editbtn)
        editText.setAttribute("onclick", "editFunction("+index+")")
        icon.setAttribute("onclick", "deleteTask("+index+")")
        complete.setAttribute("onclick", "taskCompleted("+index+")")
        var br = document.createElement("br");
        complete.setAttribute("type", "checkbox");
        li.setAttribute("id", index);
        ul.appendChild(complete);
        ul.appendChild(li);
        li.appendChild(taskNode);
        ul.appendChild(icon);
        icon.setAttribute("class", "fa fa-trash");
        ul.appendChild(editText);
        ul.appendChild(br);
        
    }
    
    
}

process_to_dos();

// localStorage.setItem('mytodos', null);